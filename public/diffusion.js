let l = 10; //matrix size
let w = 20 //square size
let n = 5; //number of particles
let f = 5;
let c = 0;
let R = [];
let G = [];
let B = [];
let grid = [];
let X = [];
let Y = [];

function start(){

   l =  document.getElementById('Size').value;
   n =  document.getElementById('ParticleNo').value;
   f =  document.getElementById('Speed').value;
   setup();
   loop();


}
function stop(){
   noLoop();
}

function RandomPosition() {

	var position = Math.floor(Math.random() * ((l+2) - 2)) + 1;
	return position;
}

function RandomStep(){

	let step;
	var direction = Math.floor(Math.random()* (2 - 0 + 1)) + 0;
	if(direction === 2) {
		direction = -1;
	}

	step = direction * w;
	// console.log(step);
	return step;
}



function setup() {


//fill grid with zeros
for(let k = 0; k<l;k++){
  for(var i =0; i<l;i++){
        grid[c] = {"X":k*w+(w/2), "Y":i*w+(w/2), "isOccupied":0};
        c++;

  }
}

for(let h = 0; h<n; h++){

R[h] = random(255);
 G[h] = random(255);
 B[h] = random(255);
}

var canvas = createCanvas(w*l, w*l);
  noLoop();
  canvas.parent('animation');
}


function draw() {

    frameRate(f);
    fill(255);
    rect(0,0, width-1, height-1);
    background(255, 255, 255, 0.0);
  	//For (var BEGIN; END; INTERVAL){
  	//DO SOMETHING }
  	//DRAW GRID
  	for (var x = 0; x < width; x += width / l) {
  		for (var y = 0; y < height; y += height / l) {
  			stroke(0);
  			strokeWeight(1);
  			line(x, 0, x, height);
  			line(0, y, width, y);
  		}
  	}

  	drawParticle();
}

	let posX, posY;
	let newX, newY, OldX, OldY;
for(let i = 0; i<n; i++){
		posX = RandomPosition();
		posY = RandomPosition();
		//get random position for every particle
		//Transfer it to location in pixels
	X[i] = (posX-1)*w + (w/2);
	Y[i] = (posY-1)*w + (w/2);
	 //find the address of the particle in the Grid
	let posInGrid = (l*(posX-1) + posY-1);
		//update Occupied data
	grid[posInGrid] = {"X":X[i], "Y":Y[i], "isOccupied":1};
	// console.log(X[i], Y[i], posInGrid);
	// console.log(grid[posInGrid]);
	}

function drawParticle(){
	moveParticle();
	// console.log(RandomPosition());
}

function moveParticle() {


	for(let i = 0; i<n; i++){
		fill(R[i], G[i], B[i]);
    //find position in Grid
		let posInGrid = (l*((X[i]-(w/2))/w-1+1) + (Y[i]-(w/2))/w-1+1);
    grid[posInGrid] = {"X": X[i], "Y": Y[i], "isOccupied":1};
		newX = X[i] + RandomStep();
		newY = Y[i] + RandomStep();

		//WARUNKI BRZEGOWE
		if(newX>=l*w){
		newX = w/2;
		}

		if(newY>=l*w){
		newY = w/2;
		}

		if(newX<0){
		newX = l*w - w/2;
		}

		if(newY<0){
		newY = l*w - w/2;
		}

		//find position in Grid
	 	posInGrid = (l*((newX-(w/2))/w-1+1) + (newY-(w/2))/w-1+1);
		// console.log(grid[posInGrid], newX, newY);
		// if(!grid[posInGrid]){
		// console.log("gon sie");
		// }
		while(grid[posInGrid].isOccupied === 1) {
		newX = X[i] + RandomStep();
		newY = Y[i] + RandomStep();
    		//WARUNKI BRZEGOWE
		if(newX>=l*w){
		newX = w/2;
		}

		if(newY>=l*w){
		newY = w/2;
		}

		if(newX<0){
		newX = l*w - w/2;
		}

		if(newY<0){
		newY = l*w - w/2;
		}

		posInGrid = (l*((newX-(w/2))/w-1+1) + (newY-(w/2))/w-1+1);
		}


		grid[posInGrid] = {"X": newX, "Y": newY, "isOccupied":1}
		//clear last position
		let posInGridOld = (l*((X[i]-w/2)/w-1+1) + (Y[i]-w/2)/w-1+1);
    // console.log(grid[posInGridOld], posInGridOld, X[i], Y[i]);
		grid[posInGridOld] = {"X": X[i], "Y": Y[i], "isOccupied":0};



		ellipse(X[i],Y[i], 10,10);

			X[i] = newX;
    	Y[i] = newY;



	}
}
