//GENERUJ LOSOWY KROK : [0,-1,1]
function StepRandom() {
    var z = Math.floor(3 * Math.random());
    if(z == 2){ //powyższa funkcja generowała liczby 0,1,2 więc te ostatnią zamieniamy na -1
      z = -1;
    }

    return z;
}

//GENERUJ LOSOWY KROK : [-1,1]
function OneDimensionStepRandom() {
    var z = Math.floor(2 * Math.random());
    if(z == 0){ //powyższa funkcja generowała liczby 1,0 więc te ostatnią zamieniamy na -1
      z = -1;
    }
    return z;
}

function randomWalk(steps) {
    steps = document.getElementById("PointNumber").value; //Definiowana przez użytkownika liczba kroków naszego pijaka, domyślnie 1000
    var points = [],
        x_value = 0, //początkowa wartość x
        y_value = 0, //początkowa wartość y
        t;

    for (t = 0; t < steps; t += 1) {
        x_value += StepRandom(); //ruch w osi x
        y_value += StepRandom() //ruch w osi y
        points.push([x_value, y_value]); // zapisujemy wartości do tablicy współrzędnych do wykresu
    }
    return points;
}

function OneDimensionRandomWalk(steps) {
    steps = document.getElementById("PointNumber").value; //Definiowana przez użytkownika liczba kroków naszego pijaka, domyślnie 1000
    var points = [],
        y_value = 0, //początkowa wartość x
        t;

    for (t = 0; t < steps; t += 1) {
        y_value += OneDimensionStepRandom(); //ruch w osi y
        points.push([t, y_value]); // zapisujemy wartości do tablicy współrzędnych do wykresu
    }
    return points;
}

/// Funkcja zwracająca LN(N) i LN(SIGMA)
function LogChart(steps){
  steps = 100;
  var points = [],
      final_location=0,
      y_value = 0, //początkowa wartość y
      sum = 0,
      squares_sum = 0,
      sum_diff = 0,
      sigma = 0,
      lnSigma,
      lnSteps,
      i,
      t;

//PĘTLA ZWIĘKSZAJĄCA ILOŚĆ PIJAKÓW
for(n=2; n < steps; n++){
  final_location = 0;
  sum=0;
  y_value = 0;
  squares_sum = 0;
  //JEDNOWYMIAROWE PIJAKI
  for(i=0; i<n; i++) {
    for (t = 0; t < steps; t += 1) {
        y_value += OneDimensionStepRandom(); //ruch w osi y
      }
       final_location = y_value;
       sum += final_location;
       squares_sum = final_location*final_location;
       console.log("sum",sum, squares_sum);
}

sum = Math.pow((sum/n),2);
squares_sum = squares_sum/n;
console.log("sum2", sum ,"squares_sum", squares_sum);
sigma = Math.pow((Math.abs(squares_sum - sum)), 0.5);
console.log("sigma",sigma);
lnSigma=Math.log(sigma);
lnSteps=Math.log(n);
points.push([lnSteps, lnSigma]); // zapisujemy wartości do tablicy współrzędnych do wykresu


}
  console.log(points);
  return points;
}


function generateLogPlot(plotNum) { //generowanie wykresu ruchu pijaka
    var plots = [],
        index;

    for (index = 0; index < plotNum; index += 1) {
        plots.push({
            regression:true,
            name: 'plot' + index,
            data:LogChart()
        });
    }
    return plots;
}

function generatePlots(plotNum) { //generowanie wykresu ruchu pijaka
    var plots = [],
        index;

    for (index = 0; index < plotNum; index += 1) {
        plots.push({
            name: 'plot' + index,
            data:randomWalk()
        });
    }
    return plots;
}

function OneDimensionGeneratePlots(plotNum) { //generowanie wykresu ruchu pijaka
    var plots = [],
        index;

    for (index = 0; index < plotNum; index += 1) {
        plots.push({
            name: 'plot' + index,
            data:OneDimensionRandomWalk()
        });
    }
    return plots;
}

function GenerateRegression(plotNum) { //generowanie wykresu regresji liniowej ruchu pijaka
    var plots = [],
        index;

    for (index = 0; index < plotNum; index += 1) {
        plots.push({
            regression:true,
            name: 'plot' + index,
            color:'rgba(0,0,0,0.05)',
            data:randomWalk()
        });
    }
    return plots;
}

//Wyświetlanie wykresów korzystając z paczki JavaScript - Highcharts.js

function refreshPlot(){
      console.log(generatePlots(WalkerNumber));
      var WalkerNumber = document.getElementById("WalkerNumber").value;
      var myChart = Highcharts.chart('pijaczyna', {

          title: {
              text: 'Walking Patterns'
          },
          xAxis: {
            type:'linear'
          },
          yAxis: {
              title: {
                  text: 'Value'
              },
              plotLines: [{
                  value: 0,
                  width: 1,
                  color: '#808080'
              }]
          },
          tooltip: {
              valueSuffix: ' units'
          },
          series: generatePlots(WalkerNumber)
      });

}

function PlotLogChart(){
      console.log(generatePlots(WalkerNumber));
      var WalkerNumber = document.getElementById("WalkerNumber").value;
      var myChart = Highcharts.chart('pijaczyna', {

          title: {
              text: 'Log(StdDeviation)'
          },
          xAxis: {
            type:'linear',
            title:{text:'ln(N)'},
          },
          yAxis: {
              title: {
                  text: 'ln(StdDeviation)'
              },
              plotLines: [{
                  value: 0,
                  width: 1,
                  color: '#808080'
              }]
          },
          tooltip: {
              valueSuffix: ' units'
          },
          series: generateLogPlot(WalkerNumber)
      });

}



function OneDimensionRefreshPlot(){
      var WalkerNumber = document.getElementById("WalkerNumber").value;
      var myChart = Highcharts.chart('pijaczyna', {

          title: {
              text: 'Walking Patterns'
          },
          xAxis: {
            type:'linear'
          },
          yAxis: {
              title: {
                  text: 'Value'
              },
              plotLines: [{
                  value: 0,
                  width: 1,
                  color: '#808080'
              }]
          },
          tooltip: {
              valueSuffix: ' units'
          },
          series: OneDimensionGeneratePlots(WalkerNumber)
      });

}


function RegressionPlot(){

  var WalkerNumber = document.getElementById("WalkerNumber").value;
  var myChart = Highcharts.chart('pijaczyna', {
      chart:{
        type:'scatter'
      },
      title: {
          text: 'Regression'
      },
      series: GenerateRegression(WalkerNumber)
  });

}

document.addEventListener('DOMContentLoaded', function () {
    var myChart = Highcharts.chart('pijaczyna', {

        title: {
            text: 'Walking Patterns'
        },
        xAxis: {
          title: {
              text: 'Number of Steps'
          },

        },
        yAxis: {
            title: {
                text: 'Distance from Origin'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' units'
        },
        series: generatePlots(1)
    });
});
