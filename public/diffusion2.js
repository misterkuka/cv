let l = 25; //matrix size
let w = 2 //square size
let n = 3; //number of particles
let c = 0;
let steps = 100;
let grid = [];

let X = [];
let Y = [];

let initialX = [];
let initialY = [];

let finalX = [];
let finalY = [];

let diffusionGraph = [];


function RandomPosition() {

	var position = Math.floor(Math.random() * ((l+2) - 2)) + 1;
	return position;
}

function RandomStep(){
	let step;
	var direction = Math.floor(Math.random()* (2 - 0 + 1)) + 0;
	if(direction === 2) {
		direction = -1;
	}

	step = direction * w;
	return step;
}



function setup() {
//fill grid with zeros
for(let k = 0; k<l;k++){
  for(var i =0; i<l;i++){
        grid[c] = {"X":k*w+(w/2), "Y":i*w+(w/2), "isOccupied":0};
        c++;
  }
}
}

function averageDisplacement(){
  let displacement = [];
  let averageDisplacement = 0;
  for(let g = 0; g<n; g++){
    let disX = Math.abs(initialX[g]) - Math.abs(finalX[g]);
    let disY = Math.abs(initialY[g]) - Math.abs(finalY[g]);
    let disXY = Math.pow(disX, 2) + Math.pow(disY, 2);
    displacement[g] = Math.pow(disXY, 0.5);
    averageDisplacement += Math.pow(disXY, 0.5);
  }
  averageDisplacement = averageDisplacement/n;
  return averageDisplacement;
}



function draw() {
  let avgR = 0;
  for(let i = 0; i < steps; i++) {

//SET THE PARTICLES IN RANDOM PLACES
  let posX, posY;
  for(let i = 0; i<n; i++){
  		posX = RandomPosition();
  		posY = RandomPosition();
  		//get random position for every particle
  		//Transfer it to location in pixels
  	X[i] = (posX-1)*w + (w/2);
  	Y[i] = (posY-1)*w + (w/2);
  	 //find the address of the particle in the Grid
  	let posInGrid = (l*(posX-1) + posY-1);
  		//update Occupied data
  	grid[posInGrid] = {"X":X[i], "Y":Y[i], "isOccupied":1};
  	// console.log(X[i], Y[i], posInGrid);
  	// console.log(grid[posInGrid]);
  }

  for(let i = 0; i < steps; i++){
    moveParticle();
    if(i===1){
      for(let g = 0; g<n; g++){
          initialX[g] = X[g];
          initialY[g] = Y[g];
      }
    }
  }

  for(let g = 0; g<n; g++){
      finalX[g] = X[g];
      finalY[g] = Y[g];
  }

  avgR += averageDisplacement();
  console.log(avgR/(i+1));
  diffusionGraph.push([steps*(i+1), Math.pow(avgR/(i+1),2)/(4*steps)]);

  }
  console.log(diffusionGraph);
}

function moveParticle() {
	for(let i = 0; i<n; i++){
    //find position in Grid
		let posInGrid = (l*((X[i]-(w/2))/w-1+1) + (Y[i]-(w/2))/w-1+1);
    grid[posInGrid] = {"X": X[i], "Y": Y[i], "isOccupied":1};
		newX = X[i] + RandomStep();
		newY = Y[i] + RandomStep();

		//WARUNKI BRZEGOWE
		if(newX>=l*w){
		newX = w/2;
		}

		if(newY>=l*w){
		newY = w/2;
		}

		if(newX<0){
		newX = l*w - w/2;
		}

		if(newY<0){
		newY = l*w - w/2;
		}

		//find position in Grid
	 	posInGrid = (l*((newX-(w/2))/w-1+1) + (newY-(w/2))/w-1+1);
		// console.log(grid[posInGrid], newX, newY);
		// if(!grid[posInGrid]){
		// console.log("gon sie");
		// }
		while(grid[posInGrid].isOccupied === 1) {
		newX = X[i] + RandomStep();
		newY = Y[i] + RandomStep();
    		//WARUNKI BRZEGOWE
		if(newX>=l*w){
		newX = w/2;
		}

		if(newY>=l*w){
		newY = w/2;
		}

		if(newX<0){
		newX = l*w - w/2;
		}

		if(newY<0){
		newY = l*w - w/2;
		}

		posInGrid = (l*((newX-(w/2))/w-1+1) + (newY-(w/2))/w-1+1);
		}


		grid[posInGrid] = {"X": newX, "Y": newY, "isOccupied":1}
		//clear last position
		let posInGridOld = (l*((X[i]-w/2)/w-1+1) + (Y[i]-w/2)/w-1+1);
    // console.log(grid[posInGridOld], posInGridOld, X[i], Y[i]);
		grid[posInGridOld] = {"X": X[i], "Y": Y[i], "isOccupied":0};

			X[i] = newX;
    	Y[i] = newY;



	}
}

document.addEventListener('DOMContentLoaded', function () {
  console.log('elo');
  setup();
  draw();

  var myChart = Highcharts.chart('pijaczyna', {
      chart: {
        type:"scatter"
      },
      title: {
          text: 'Diffusion Coefficient Chart'
      },
      xAxis: {
        title: {
            text: 'Number of Steps'
        },

      },
      yAxis: {
          title: {
              text: 'Diffusion Coefficient'
          },
          plotLines: [{
              value: 0,
              width: 1,
              color: '#808080'
          }]
      },
      tooltip: {
          valueSuffix: ' units'
      },
      series: [{
        data:diffusionGraph
      }]
  });
});
