const express = require('express');
var favicon = require('serve-favicon');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const { check, validationResult } = require('express-validator');
const nodemailer = require('nodemailer');
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
var server = require('http').createServer(app);



app.use(express.static(path.join(__dirname, 'public')));
// app.use('/ui', express.static(__dirname + '/node_modules/materialize-css/'));

// PARTICLES

// EMAIL TRANSPORTER
let transporter = nodemailer.createTransport({
  service: 'SendinBlue',
  auth: {
    user: 'nibojczuk@gmail.com',
    pass: 'fxUVMZGj5npwbWT7'
  }
});

// Express Messages Middleware
app.use(require('connect-flash')());
app.use(function(req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

// HOME ROUTE
app.get('/', function(req, res) {
  res.render('landing');
});
// galeria
const testFolder = 'public/gal/';
const fs = require('fs');


app.get('/photos', function(req, res) {
  fs.readdir(testFolder, (err, files) => {
      res.render('galeria', {
        photos:files
      });
  });
});
app.get('/projects', function(req, res) {
  res.render('projects');
});

app.get('/contact', function(req, res) {
  res.render('contact');
});

app.get('/projects/pijak', function(req,res){
  res.render('pijak_live');
});

app.get('/projects/diffusion', function(req,res){
  res.render('diffusion_live');
});

app.get('/projects/diffusion2', function(req,res){
  res.render('difussion_live2');
});

app.get('/projects/diffusion3', function(req,res){
  res.render('difussion_live3');
});

app.get('/projects/analog_pc_live', function(req,res){
  res.render('analog_pc_live');
});

app.post('/email', [check('title', 'Enter mail title').notEmpty(), check('email', 'Wrong email adress').isEmail(), check('content', 'Email content empty').notEmpty()],function(req, res) {
  const email = req.body.email;
  const title = req.body.title;
  const content = req.body.content;

  let errors = validationResult(req);
  console.log(errors)
  if (errors.errors.length>0) {
    res.render('contact', {
      errors: errors,
    });
  } else {
    let mailOptions = {
      from: email, // sender address
      to: 'nibojczuk@gmail.com, ' + email, // list of receivers
      subject: title, // Subject line
      text: "From: " + email + " " + content, // plain text body
    };
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
      }
      console.log('Message %s sent: %s', info.messageId, info.response);
    });
    let alerts = "Email sent";
    res.render('email_sent', {
      alerts: alerts
    })
  }
});

app.get('/Bialystok' || '/BIALYSTOK' || "/bialystok", (req, res) => {
  res.redirect("https://drive.google.com/drive/folders/1_3gYWcgE1c5z7oJ578t77L0Zm99nDPbK?usp=sharing")
})

app.get('*', function(req, res) {
  res.redirect('/');
});

// load view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// START server
server.listen(80);
//socket
